package maestro.likeswipe;

import android.animation.Animator;
import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObservable;
import android.database.DataSetObserver;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.Scroller;

import java.util.Stack;

/**
 * Created by Artyom on 05.03.2016.
 */
public class CardStackView extends ViewGroup {

    public static final String TAG = CardStackView.class.getSimpleName();

    private static final int NOT_SET = -1;

    private static final int SWIPE_MIN_DISTANCE = 120;
    private static final int SWIPE_MAX_OFF_PATH = 250;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;

    private static final int DEF_ANIMATION_DURATION = 3600;

    private static final float DEF_DEGREES = 35f;

    private CardStackAdapter mAdapter;
    private OnScrollListener mScrollListener;
    private OnItemClickListener mItemClickListener;
    private OnItemLongClickListener mLongClickListener;

    private RecycleBin mRecycle = new RecycleBin();
    private DataObserver mObserver = new DataObserver();
    private CardAnimator mAnimator;

    private View mEmptyView;

    private GestureDetector mDetector;

    private int mStackCount = 3;
    private int mVisibleSize = 0;
    private int mCurrentPosition = NOT_SET;

    private int mClipPaddingLeft;
    private int mClipPaddingTop;
    private int mClipPaddingRight;
    private int mClipPaddingBottom;

    private float mDegrees = DEF_DEGREES;
    private float mOutPercents = 0.5f;
    private float mSpeedFactor = 2f;

    private boolean isDebug = true;
    private boolean isViewBoundsPrimary = true;

    private float diffX;
    private float diffY;
    private float mPercentsX;
    private float mPercentsY;
    private float mRotation;
    private boolean isFling = false;
    private boolean isFirstTime = true;
    private boolean isScrolling = false;
    private boolean isTouchOnTop = false;
    private boolean swapAfterAnimate = false;
    private boolean notifyEndAfterAnimate = false;

    private FlingRunnable mRunnable;
    private Direction mSwitchDirection;
    private Direction mVerticalDirection;
    private Direction mHorizontalDirection;

    private FlingRunnable.OnFlingProgressListener mRunnableListener = new FlingRunnable.OnFlingProgressListener() {
        @Override
        public void onScroll(int x, int y) {
            applyDiffs(x, y);
        }

        @Override
        public void onScrollEnd() {
            if (swapAfterAnimate) {
                next();
            } else if (notifyEndAfterAnimate) {
                if (mScrollListener != null) {
                    mScrollListener.onSwapEnd(getChildAt(0), mSwitchDirection);
                }
            }
            clean();
        }

        @Override
        public void onScrollCancel() {
            onScrollEnd();
        }
    };

    public enum Direction {
        Left, Top, Right, Bottom;

        static boolean isHorizontalDirection(Direction direction) {
            return direction == Left || direction == Right;
        }

    }

    public CardStackView(Context context) {
        super(context);
        init(null);
    }

    public CardStackView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public CardStackView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    void init(AttributeSet attrs) {

        mDetector = new GestureDetector(getContext(), mListener);
        mVisibleSize = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, getResources().getDisplayMetrics());

        setChildrenDrawingOrderEnabled(true);

        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.CardStackView);
            mClipPaddingLeft = mClipPaddingTop = mClipPaddingRight = mClipPaddingBottom
                    = a.getDimensionPixelSize(R.styleable.CardStackView_clipPadding, 0);

            mClipPaddingLeft = a.getDimensionPixelSize(R.styleable.CardStackView_clipPadding_left, mClipPaddingLeft);
            mClipPaddingTop = a.getDimensionPixelSize(R.styleable.CardStackView_clipPadding_top, mClipPaddingTop);
            mClipPaddingRight = a.getDimensionPixelSize(R.styleable.CardStackView_clipPadding_right, mClipPaddingRight);
            mClipPaddingBottom = a.getDimensionPixelSize(R.styleable.CardStackView_clipPadding_bottom, mClipPaddingBottom);

            mStackCount = Math.max(1, a.getInteger(R.styleable.CardStackView_stackCount, mStackCount));
            mOutPercents = a.getFloat(R.styleable.CardStackView_outPercents, mOutPercents);
            mVisibleSize = a.getDimensionPixelSize(R.styleable.CardStackView_visibleSize, mVisibleSize);

            mDegrees = a.getFloat(R.styleable.CardStackView_degrees, mDegrees);
            mSpeedFactor = a.getFloat(R.styleable.CardStackView_speedFactor, mSpeedFactor);

            a.recycle();
        }
        mStackCount += 1;
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (mAdapter != null) {
            mAdapter.unregisterObserver(mObserver);
        }
        mRecycle.clear();
    }

    @Override
    protected int getChildDrawingOrder(int childCount, int i) {
        return childCount - i - 1;
    }

    @Override
    protected LayoutParams generateDefaultLayoutParams() {
        return new CardLayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        if (isDebug) {
            Rect rect = new Rect(mClipPaddingLeft, mClipPaddingTop, getWidth() - getPaddingLeft() - mClipPaddingRight, getHeight() - getPaddingBottom() - mClipPaddingBottom);
            Paint paint = new Paint();
            paint.setStyle(Paint.Style.FILL);
            paint.setColor(mRunnable != null ? Color.RED : Color.GREEN);
            canvas.drawRect(rect, paint);
        }
        super.dispatchDraw(canvas);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);

        int maxWidth = width - mClipPaddingLeft - mClipPaddingRight;
        int maxHeight = height - mClipPaddingTop - mClipPaddingBottom;

        int horizontalPadding = mClipPaddingLeft + mClipPaddingRight + getPaddingLeft() + getPaddingRight();
        int verticalPadding = mClipPaddingTop + mClipPaddingBottom + getPaddingTop() + getPaddingBottom() + mVisibleSize * (mStackCount - 2);

        if (isEmpty()) {
            measureChildren(
                    MeasureSpec.makeMeasureSpec(maxWidth, widthMode),
                    MeasureSpec.makeMeasureSpec(maxHeight, heightMode));
        } else {
            measureChildren(
                    getChildMeasureSpec(widthMeasureSpec, horizontalPadding, MarginLayoutParams.MATCH_PARENT),
                    getChildMeasureSpec(heightMeasureSpec, verticalPadding, MarginLayoutParams.MATCH_PARENT));
        }
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {

        final int childCount = getChildCount();

        final int width = r - l;

        final int startX = mClipPaddingLeft + getPaddingLeft();
        final int startY = mClipPaddingTop + getPaddingTop();

        CardLayoutParams previousParams = null;

        for (int i = 0; i < childCount; i++) {
            final View child = getChildAt(i);
            final CardLayoutParams params = (CardLayoutParams) child.getLayoutParams();

            if (i == childCount - 1 && childCount == mStackCount) {
                params.copyFrom(previousParams);
            } else {
                final int offset = mVisibleSize * i;
                final float scale = (width - offset) / (float) width;
                final int translation = getScaleTranslation(child, scale);

                params.setScale(scale);
                params.setOffset(offset);
                params.setTranslation(translation);
                params.setTargetParams(previousParams);
                previousParams = params;
            }

            int x = startX;
            int fx = x + child.getMeasuredWidth();

            int y = startY + params.getTranslatedOffset();
            int fy = y + child.getMeasuredHeight();
            child.layout(x, y, fx, fy);

            params.resetChild(child);

        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (mRunnable != null) {
            return false;
        }
        if (!isEmpty() && getChildCount() > 0) {
            mDetector.onTouchEvent(event);
            switch (event.getAction() & event.getActionMasked()) {
                case MotionEvent.ACTION_CANCEL:
                case MotionEvent.ACTION_UP:
                    onTouchEnd();
                    break;
            }
        }
        return isScrolling || super.onTouchEvent(event);
    }

    @Override
    public boolean canScrollHorizontally(int direction) {
        return super.canScrollHorizontally(direction);
    }

    @Override
    public boolean canScrollVertically(int direction) {
        return super.canScrollVertically(direction);
    }

    private void applyDiffs(float xDiff, float yDiff) {
        final View v = getChildAt(0);
        final float affectDegrees = isTouchOnTop ? -mDegrees : mDegrees;

        final int width, height;
        if (isViewBoundsPrimary) {
            width = v.getMeasuredWidth();
            height = v.getMeasuredHeight();
        } else {
            width = getMeasuredWidth();
            height = getMeasuredHeight();
        }

        diffX = xDiff;
        diffY = yDiff;

        mPercentsX = Math.max(-1f, Math.min(1f, diffX / width));
        mPercentsY = Math.max(-1f, Math.min(1f, diffY / height));
        mRotation = affectDegrees * (diffX / getWidth());

        v.setTranslationX(diffX);
        v.setTranslationY(diffY);
        v.setRotation(mRotation);

        final float affectPercents = Math.min(1f, Math.max(Math.abs(mPercentsX), Math.abs(mPercentsY)) * mSpeedFactor);

        int childCount = getChildCount();
        if (childCount == mStackCount) {
            childCount--;
        }

        for (int i = 1; i < childCount; i++) {
            final View child = getChildAt(i);
            final CardLayoutParams params = (CardLayoutParams) child.getLayoutParams();
            final float affectScale = params.affectScale(affectPercents);
            final float affectOffset = params.affectOffset(affectPercents);

            child.setScaleY(affectScale);
            child.setScaleX(affectScale);
            child.setTranslationY(affectOffset);
            child.setTranslationX(0);
        }

        if (mScrollListener != null) {
            mScrollListener.onScroll(v, mPercentsX, mPercentsY);
        }

        invalidate();
    }

    private void onTouchEnd() {
        final View view = getChildAt(0);

        isFirstTime = false;

        float absPercentX = Math.abs(mPercentsX);
        float absPercentY = Math.abs(mPercentsY);

        if (!isFling) {
            mHorizontalDirection = mPercentsX > 0 ? Direction.Right : Direction.Left;
            mVerticalDirection = mPercentsY > 0 ? Direction.Bottom : Direction.Top;
            if (absPercentX > absPercentY
                    || (mScrollListener != null
                    && !mScrollListener.canSwipe(view, mVerticalDirection)
                    && mScrollListener.canSwipe(view, mHorizontalDirection))) {
                if (absPercentX >= mOutPercents) {
                    mSwitchDirection = mHorizontalDirection;
                }
            } else if (absPercentY >= mOutPercents) {
                mSwitchDirection = mVerticalDirection;
            }
        }

        if (mSwitchDirection == null || (mScrollListener != null && !mScrollListener.canSwipe(view, mSwitchDirection))) {
            mRunnable = new FlingRunnable(getContext(), this, mRunnableListener, FlingRunnable.FlingType.SCROLL);
            mRunnable.scroll((int) diffX, (int) diffY, 0, 0);
        } else if (!isFling) {
            animateNext(mSwitchDirection);
        }

    }

    public void animateNext(Direction direction) {
        if (!hasNext()) {
            return;
        }

        stopCurrentScroll();

        mSwitchDirection = direction;
        mRunnable = new FlingRunnable(getContext(), this, mRunnableListener, FlingRunnable.FlingType.FLING);
        int xEnd = (int) diffX;
        int yEnd = (int) diffY;
        final View v = getChildAt(0);
        if (mScrollListener != null) {
            mScrollListener.onSwipeStart(v, direction);
        }
        if (Direction.isHorizontalDirection(direction)) {
            xEnd = getOutSize(v, direction, getMinRotateDegrees());
        } else {
            yEnd = getOutSize(v, direction, v.getRotation());
        }
        swapAfterAnimate = true;
        mRunnable.scroll((int) diffX, (int) diffY, xEnd, yEnd, DEF_ANIMATION_DURATION);
    }

    public void animatePrevious(Direction direction) {
        if (!hasPrevious()) {
            return;
        }

        stopCurrentScroll();
        notifyEndAfterAnimate = true;
        mSwitchDirection = direction;
        mRunnable = new FlingRunnable(getContext(), this, mRunnableListener, FlingRunnable.FlingType.SCROLL);
        previous(true);

        int childIndex = 0;
        final View v = getChildAt(childIndex);

        if (mScrollListener != null) {
            mScrollListener.onSwipeStart(v, direction);
        }

        mRunnable.scroll((int) diffX, (int) diffY, 0, 0, DEF_ANIMATION_DURATION);

    }

    public void next() {
        swapAfterAnimate = false;
        if (isEmpty()) {
            checkEmptyView();
        } else if (hasNext()) {
            mCurrentPosition++;

            int childIndex = 0;
            View currentView = getChildAt(childIndex);
            if (currentView != null) {
                removeViewAt(childIndex);
            } else {
                currentView = mRecycle.peekScrapView();
                if (currentView == null) {
                    currentView = mAdapter.createView();
                }
            }

            int nextPosition = mCurrentPosition + mStackCount - 1;

            if (nextPosition < mAdapter.getCount()) {
                mAdapter.bindView(currentView, nextPosition);
                addView(currentView);
            } else {
                mRecycle.addScrapView(currentView);
            }
            if (mScrollListener != null && mSwitchDirection != null) {
                mScrollListener.onSwapEnd(getChildAt(0), mSwitchDirection);
            }
        }
    }

    public void previous() {
        previous(false);
    }

    public void previous(boolean byAnimation) {
        if (isEmpty()) {
            checkEmptyView();
        } else if (hasPrevious()) {
            mCurrentPosition--;

            int childIndex = getChildCount() - 1;
            View lastView = getChildAt(childIndex);
            if (lastView != null && getChildCount() == mStackCount) {
                removeViewAt(childIndex);
            } else {
                lastView = mRecycle.peekScrapView();
                if (lastView == null) {
                    lastView = mAdapter.createView();
                }
            }
            mAdapter.bindView(lastView, mCurrentPosition);

            if (byAnimation) {
                if (Direction.isHorizontalDirection(mSwitchDirection)) {
                    diffX = getOutSize(lastView, mSwitchDirection, getMinRotateDegrees());
                    diffY = 0;
                } else {
                    diffX = 0;
                    diffY = getOutSize(lastView, mSwitchDirection, 0);
                }
                lastView.setTranslationX(diffX);
                lastView.setTranslationY(diffY);
                lastView.setRotation(diffX / lastView.getMeasuredWidth() * mDegrees);
            }

            addView(lastView, 0);

            if (mScrollListener != null && !notifyEndAfterAnimate) {
                mScrollListener.onSwapEnd(lastView, mSwitchDirection);
            }
        }
    }

    public boolean hasNext() {
        return mCurrentPosition < getCount() - 1;
    }

    public boolean hasPrevious() {
        return mCurrentPosition > 0;
    }

    private boolean isEmpty() {
        return mAdapter == null
                || mAdapter.getCount() < 0
                || mCurrentPosition >= mAdapter.getCount()
                || mCurrentPosition < 0;
    }

    private void fill() {
        boolean doRequest = false;
        if (mAdapter == null || mAdapter.getCount() == 0) {
            if (getChildCount() > 0) {
                removeAllViews();
                doRequest = true;
            }
        } else {
            removeAllViews();
            for (int i = 0; i < mStackCount; i++) {
                View view = mRecycle.peekScrapView();
                if (view == null) {
                    view = mAdapter.createView();
                }
                mAdapter.bindView(view, mCurrentPosition + i);
                addView(view);

                if (mAnimator != null) {
                    mAnimator.animateAppear(view, mCurrentPosition + i);
                }
            }
        }
        if (doRequest) {
            requestLayout();
            invalidate();
        }
    }

    private void clean() {
        isFling = false;
        isFirstTime = true;
        isScrolling = false;
        isTouchOnTop = false;
        diffX = 0;
        diffY = 0;
        mPercentsX = 0;
        mPercentsY = 0;
        mRotation = 0;
        mRunnable = null;
        mSwitchDirection = null;
        notifyEndAfterAnimate = false;
        swapAfterAnimate = false;
    }

    private void stopCurrentScroll() {
        if (mRunnable != null) {
            mRunnable.cancel();
        }
    }

    public void setAdapter(CardStackAdapter adapter) {
        if (mAdapter != null) {
            mAdapter.unregisterObserver(mObserver);
        }
        mAdapter = adapter;
        if (mAdapter != null) {
            mAdapter.registerObserver(mObserver);
        }
        mCurrentPosition = 0;
        fill();
        checkEmptyView();
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mItemClickListener = listener;
    }

    public void setOnItemLongClickListener(OnItemLongClickListener listener) {
        mLongClickListener = listener;
    }

    public void setOnScrollListener(OnScrollListener listener) {
        mScrollListener = listener;
    }

    public CardStackAdapter getAdapter() {
        return mAdapter;
    }

    public int getCount() {
        return mAdapter != null ? mAdapter.getCount() : 0;
    }

    public void setEmptyView(View v) {
        mEmptyView = v;
    }

    private void checkEmptyView() {
        if (mEmptyView != null) {
            mEmptyView.setVisibility(isEmpty() ? View.VISIBLE : View.GONE);
        }
    }

    private int getScaleTranslation(View view, float scale) {
        return Math.round(view.getMeasuredHeight() * (1f - scale) / 2);
    }

    private int getOutScreenWidth(View v, int increase, float degrees) {
        return v.getMeasuredWidth() + increase
                + Math.abs(getRotatedHorizontalDiffs(v, degrees));
    }

    private int getOutScreenHeight(View v, int increase, float degrees) {
        return v.getMeasuredHeight() + increase
                + Math.abs(getRotatedVerticalDiffs(v, degrees));
    }

    private float getMinRotateDegrees() {
        return Math.max(45f, mDegrees);
    }

    private int getRotatedHorizontalDiffs(View view, float angle) {
        angle = Math.abs(angle);
        Rect origin = new Rect();
        origin.right = view.getWidth();
        origin.bottom = view.getHeight();

        RectF rotated = new RectF(origin);
        Matrix matrix = new Matrix();
        matrix.postRotate(angle, view.getPivotX(), view.getPivotY());
        matrix.mapRect(rotated);

        return Math.round(origin.right - rotated.right);
    }

    private int getRotatedVerticalDiffs(View view, float angle) {
        angle = Math.abs(angle);
        Rect origin = new Rect();
        origin.right = view.getWidth();
        origin.bottom = view.getHeight();

        RectF rotated = new RectF(origin);
        Matrix matrix = new Matrix();
        matrix.postRotate(angle, view.getPivotX(), view.getPivotY());
        matrix.mapRect(rotated);

        return Math.round(origin.bottom - rotated.bottom);
    }

    private int getOutSize(View v, Direction direction, float degrees) {
        switch (direction) {
            case Left:
                return -getOutScreenWidth(v, mClipPaddingLeft + getPaddingLeft(), degrees);
            case Right:
                return getOutScreenWidth(v, mClipPaddingRight + getPaddingRight(), degrees);
            case Top:
                return -getOutScreenHeight(v, mClipPaddingTop + getPaddingTop(), degrees);
            case Bottom:
                return getOutScreenHeight(v, mClipPaddingBottom + getPaddingBottom() + mVisibleSize * (mStackCount - 2), degrees);
            default:
                return 0;
        }
    }

    private class DataObserver extends DataSetObserver {
        @Override
        public void onChanged() {
            super.onChanged();
            fill();
        }

        @Override
        public void onInvalidated() {
            super.onInvalidated();
            requestLayout();
            invalidate();
        }
    }

    public class CardLayoutParams extends LayoutParams {

        private CardLayoutParams mTargetParams;

        private int mOffset;
        private int mTranslation;
        private float mScale;

        public CardLayoutParams(LayoutParams source) {
            super(source);
        }

        public CardLayoutParams(int width, int height) {
            super(width, height);
        }

        public void setScale(float scale) {
            mScale = scale;
        }

        public float getScale() {
            return mScale;
        }

        public void setOffset(int offset) {
            mOffset = offset;
        }

        public int getOffset() {
            return mOffset;
        }

        public void setTranslation(int translation) {
            mTranslation = translation;
        }

        public int getTranslation() {
            return mTranslation;
        }

        public void setTargetParams(CardLayoutParams params) {
            mTargetParams = params;
        }

        public CardLayoutParams getTargetParams() {
            return mTargetParams;
        }

        public float getTargetScale() {
            return mTargetParams != null ? mTargetParams.getScale() : mScale;
        }

        public int getTargetOffset() {
            return mTargetParams != null ? mTargetParams.getOffset() : mOffset;
        }

        public int getTargetTranslation() {
            return mTargetParams != null ? mTargetParams.getTranslation() : mTranslation;
        }

        public void copyFrom(CardLayoutParams params) {
            mScale = params.getScale();
            mOffset = params.getOffset();
            mTranslation = params.getTranslation();
            mTargetParams = params.getTargetParams();
        }

        public int getTranslatedOffset() {
            return mOffset + mTranslation;
        }

        public int getTargetTranslatedOffset() {
            return mTargetParams != null ? mTargetParams.getTranslatedOffset() : getTranslatedOffset();
        }

        public float affectScale(float affectPercent) {
            float targetScale = getTargetScale();
            return mScale + (targetScale - mScale) * affectPercent;
        }

        public float affectOffset(float affectPercent) {
            int offset = getTranslatedOffset();
            int targetOffset = getTargetTranslatedOffset();
            return (targetOffset - offset) * affectPercent;
        }

        public void resetChild(View child) {
            child.setScaleY(mScale);
            child.setScaleX(mScale);
            child.setTranslationY(0);
            child.setTranslationX(0);
            child.setRotation(0);
        }
    }

    private class RecycleBin {

        private Stack<View> mScrapViews = new Stack<>();

        public void addScrapView(View view) {
            mScrapViews.push(view);
        }

        public void removeScrapView(View view) {
            mScrapViews.remove(view);
        }

        public View peekScrapView() {
            if (mScrapViews.isEmpty()) {
                return null;
            }
            View v = mScrapViews.peek();
            removeScrapView(v);
            return v;
        }

        public void clear() {
            mScrapViews.clear();
        }
    }

    public static class FlingRunnable implements Runnable {

        private static final int NOT_SET = -1;

        private View mView;
        private Scroller mScroller;
        private OnFlingProgressListener mListener;

        private boolean isCanceled;

        public enum FlingType {
            SCROLL, FLING
        }

        public FlingRunnable(Context context, View view, OnFlingProgressListener listener, FlingType type) {
            mView = view;
            mListener = listener;
            switch (type) {
                case SCROLL:
                    mScroller = new Scroller(context, new OvershootInterpolator(2f));
                    break;
                case FLING:
                    mScroller = new Scroller(context, new FastOutSlowInInterpolator());
                    break;
            }
        }

        public FlingRunnable(View view, OnFlingProgressListener listener, Interpolator interpolator) {
            mView = view;
            mListener = listener;
            mScroller = new Scroller(view.getContext(), interpolator);
        }

        void scroll(int xDiffStart, int yDiffStart, int xDiffEnd, int yDiffEnd) {
            scroll(xDiffStart, yDiffStart, xDiffEnd, yDiffEnd, NOT_SET);
        }

        void scroll(int xDiffStart, int yDiffStart, int xDiffEnd, int yDiffEnd, int duration) {
            mScroller.startScroll(xDiffStart, yDiffStart, xDiffEnd, yDiffEnd);
            mScroller.setFinalX(xDiffEnd);
            mScroller.setFinalY(yDiffEnd);
            if (duration != NOT_SET) {
                mScroller.extendDuration(duration);
            }
            ViewCompat.postOnAnimation(mView, this);
        }

        @Override
        public void run() {
            if (!isCanceled) {
                if (!mScroller.isFinished() && mScroller.computeScrollOffset()) {
                    int sx = mScroller.getCurrX();
                    int sy = mScroller.getCurrY();
                    mListener.onScroll(sx, sy);
                    ViewCompat.postOnAnimation(mView, this);
                } else {
                    mListener.onScrollEnd();
                }
            }
        }

        public void cancel() {
            isCanceled = true;
            mListener.onScrollCancel();
        }

        public interface OnFlingProgressListener {
            void onScroll(int x, int y);

            void onScrollEnd();

            void onScrollCancel();
        }

    }

    private GestureDetector.OnGestureListener mListener = new GestureDetector.SimpleOnGestureListener() {

        @Override
        public boolean onDown(MotionEvent e) {
            if (!isScrolling && !isEmpty()) {
                View v = getChildAt(0);
                Rect rect = new Rect();
                v.getHitRect(rect);
                isScrolling = rect.contains((int) e.getX(), (int) e.getY());
                isTouchOnTop = e.getY() - rect.top < rect.centerY();
                return true;
            }
            return super.onDown(e);
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            if (isScrolling) {
                if (isFirstTime) {
                    isFirstTime = false;
                } else {
                    applyDiffs(diffX - distanceX, diffY - distanceY);
                }
            }
            return isScrolling;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            if ((Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH
                    && Math.abs(e1.getX() - e2.getX()) > SWIPE_MAX_OFF_PATH)
                    || (Math.abs(velocityX) < SWIPE_THRESHOLD_VELOCITY
                    && Math.abs(velocityY) < SWIPE_THRESHOLD_VELOCITY)) {
                return false;
            }
            if (e1.getX() - e2.getX() < SWIPE_MIN_DISTANCE
                    && e2.getX() - e1.getX() < SWIPE_MIN_DISTANCE
                    && e1.getY() - e2.getY() < SWIPE_MIN_DISTANCE
                    && e2.getY() - e1.getY() < SWIPE_MIN_DISTANCE) {
                return false;
            }

            int childIndex = 0;
            final View v = getChildAt(childIndex);

            mHorizontalDirection = e1.getX() < e2.getX() ? Direction.Right : Direction.Left;
            mVerticalDirection = e1.getY() < e2.getY() ? Direction.Bottom : Direction.Top;

            mSwitchDirection = Math.abs((e1.getX() - e2.getX()) / v.getMeasuredWidth())
                    > Math.abs((e1.getY() - e2.getY()) / v.getMeasuredHeight())
                    ? mHorizontalDirection : mVerticalDirection;

            mRunnable = new FlingRunnable(getContext(), CardStackView.this, mRunnableListener, FlingRunnable.FlingType.FLING);

            final float k = (e2.getY() - e1.getY()) / (e2.getX() - e1.getX());

            float xEnd, yEnd;
            if (Direction.isHorizontalDirection(mSwitchDirection)) {
                xEnd = getOutSize(v, mSwitchDirection, getMinRotateDegrees());
                yEnd = k * xEnd;
            } else {
                yEnd = getOutSize(v, mSwitchDirection, v.getRotation());
                xEnd = yEnd / k;
            }

            mRunnable.scroll((int) diffX, (int) diffY, (int) xEnd, (int) yEnd, DEF_ANIMATION_DURATION);

            isFling = true;
            swapAfterAnimate = true;

            return true;
        }
    };

    public static abstract class CardStackAdapter {

        private final DataSetObservable mDataSetObservable = new DataSetObservable();

        public CardStackAdapter() {
        }

        public void registerObserver(DataSetObserver observer) {
            mDataSetObservable.registerObserver(observer);
        }

        public void unregisterObserver(DataSetObserver observer) {
            mDataSetObservable.unregisterObserver(observer);
        }

        public void notifyDataSetChanged() {
            mDataSetObservable.notifyChanged();
        }

        public void notifyDataSetInvalidated() {
            mDataSetObservable.notifyInvalidated();
        }

        public abstract int getCount();

        public abstract View createView();

        public abstract void bindView(View view, int position);

        public abstract long getItemId(int position);

    }

    public static abstract class SimpleOnScrollListener implements OnScrollListener {

        @Override
        public void onScroll(View topView, float percentX, float percentY) {
        }

        @Override
        public void onSwipeStart(View topView, Direction direction) {
        }

        @Override
        public void onSwapEnd(View topView, Direction direction) {
        }

        @Override
        public boolean canSwipe(View topView, Direction direction) {
            return true;
        }

    }

    public interface OnItemClickListener {
        void onItemClick(View v, int position, long id);
    }

    public interface OnItemLongClickListener {
        void onItemLongClickListener(View v, int position, long id);
    }

    public interface OnScrollListener {
        void onScroll(View topView, float percentX, float percentY);

        void onSwipeStart(View topView, Direction direction);

        void onSwapEnd(View topView, Direction direction);

        boolean canSwipe(View topView, Direction direction);
    }

    public interface CardAnimator {
        Animator animateAppear(View v, int position);
    }

}
