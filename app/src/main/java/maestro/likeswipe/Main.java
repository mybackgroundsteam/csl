package maestro.likeswipe;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class Main extends AppCompatActivity {

    public static final String TAG = Main.class.getSimpleName();

    private CardStackView.CardStackAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main);

        final CardStackView swipe = (CardStackView) findViewById(R.id.list);
        swipe.setAdapter(mAdapter = new TestAdapter());
        findViewById(R.id.next).setOnClickListener(new View.OnClickListener() {
            int next = 0;

            @Override
            public void onClick(View view) {
//                swipe.animateNext(CardStackView.Direction.values()[next]);
//                if (next + 1 < CardStackView.Direction.values().length) {
//                    next++;
//                } else {
//                    next = 0;
//                }
                swipe.animateNext(CardStackView.Direction.Left);
            }
        });
        findViewById(R.id.previous).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                swipe.animatePrevious(CardStackView.Direction.Bottom);
            }
        });

        swipe.setOnScrollListener(new CardStackView.SimpleOnScrollListener() {

            private View mActiveView;

            @Override
            public void onScroll(View topView, float percentX, float percentY) {
                super.onScroll(topView, percentX, percentY);

                Log.e(TAG, "onScroll: " + percentX + "/" + percentY);

                TestAdapter.Holder holder = (TestAdapter.Holder) topView.getTag();

                View newActiveView = null;
                float percents = 0;

                if (Math.abs(percentY) > Math.abs(percentX)) {
                    if (percentY < 0) {
                        newActiveView = holder.TTop;
                        percents = applyPercents(Math.abs(percentY));
                    }
                } else {
                    newActiveView = percentX > 0 ? holder.TRight : holder.TLeft;
                    percents = applyPercents(Math.abs(percentX));
                }

                Log.e(TAG, "percents: " + percents);
                Log.e(TAG, "newActiveView: " + newActiveView);

                if (mActiveView == null || !mActiveView.equals(newActiveView)) {
                    releaseActiveView();
                    mActiveView = newActiveView;
                }

                if (mActiveView != null) {
                    mActiveView.setAlpha(percents);
                }

            }

//            @Override
//            public boolean canSwipe(View topView, CardStackView.Direction direction) {
//                return direction != CardStackView.Direction.Bottom;
//            }

            private void releaseActiveView() {
                if (mActiveView != null) mActiveView.setAlpha(0);
            }

            private float applyPercents(float percents) {
                return 2f * percents;
            }

        });

    }

    public class TestAdapter extends CardStackView.CardStackAdapter {

        @Override
        public int getCount() {
            return Data.URLS.length;
        }

        @Override
        public View createView() {
            View v = LayoutInflater.from(getBaseContext()).inflate(R.layout.test, null);
            new Holder(v);
            return v;
        }

        @Override
        public void bindView(View view, int position) {
            Log.e(TAG, "bindView: " + position);
            Holder holder = (Holder) view.getTag();
            holder.reset();
            holder.Text.setText(String.valueOf(position));
            Picasso.with(getBaseContext())
                    .load(Data.URLS[position])
                    .transform(new RoundedCornersTransform(getApplicationContext()))
                    .into(holder.Image);
//                    .into(holder.Image);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public class Holder {

            TextView Text;
            ForceImageView Image;

            TextView TLeft;
            TextView TRight;
            TextView TTop;

            public Holder(View view) {
                Text = (TextView) view.findViewById(R.id.text);
                Image = (ForceImageView) view.findViewById(R.id.image);
                TLeft = (TextView) view.findViewById(R.id.tLeft);
                TRight = (TextView) view.findViewById(R.id.tRight);
                TTop = (TextView) view.findViewById(R.id.tTop);
                view.setTag(this);
            }

            public void reset() {
                TLeft.setAlpha(0);
                TRight.setAlpha(0);
                TTop.setAlpha(0);
            }

        }

    }

}
